package com.sortpack;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class CornerCasesTesting {
    private int[] arrayTest;
    private String expected;

    public CornerCasesTesting(int[] arrayTest, String expected) {
        this.arrayTest = arrayTest;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection input() {
        return Arrays.asList(new Object[][]{
                {new int[]{1, -2}, "[-2, 1]"},
                {new int[]{100, 41, -5, 0}, "[-5, 0, 41, 100]"},
                {new int[]{555, 45, -1, 1, 0, 10}, "[-1, 0, 1, 10, 45, 555]"},
                {new int[]{9, 5, 4, 1, 2, 3, 7, 6, 10, 8}, "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]"}
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testZeroArgsCase() {
        SortingApp.sort(new int[0]);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOneArgCase() {
        SortingApp.sort(new int[1]);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMoreThanTenArgsCase() {
        SortingApp.sort(new int[11]);
    }

    @Test
    public void testTwoToTenAgrsCase() {
        assertEquals(expected, SortingApp.sort(arrayTest));
    }
}
