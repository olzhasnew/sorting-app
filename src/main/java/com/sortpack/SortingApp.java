package com.sortpack;

import java.util.Arrays;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SortingApp {
    private static final Logger logger = LogManager.getLogger(SortingApp.class);

    public static void main(String[] args) {
        logger.info("Application started");
        System.out.println("Enter up to 10 ints splitting them with spacebar");

        Scanner in = new Scanner(System.in);
        String input = in.nextLine();

        String[] items = input.split(" ");
        if (items.length > 10) {
            logger.error("More than 10 arguments passed");
            throw new IllegalArgumentException("More than 10 arguments passed");
        }

        int[] arguments = new int[items.length];
        try {
            for (int i = 0; i < items.length; i++) {
                arguments[i] = Integer.parseInt(items[i]);
            }
        } catch (NumberFormatException e) {
            logger.error("Invalid input: {}", input);
            throw e;
        }

        logger.info("Sorting array: {}", Arrays.toString(arguments));
        sort(arguments);
        System.out.println(Arrays.toString(arguments));
    }

    public static String sort(int[] array) {
        if (array.length <= 1) {
            logger.error("Invalid number of arguments: {}", array.length);
            throw new IllegalArgumentException("Invalid number of arguments");
        }

        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[i]) {
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
        logger.info("Sorted array: {}", Arrays.toString(array));
        return Arrays.toString(array);
    }
}
